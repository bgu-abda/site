---
title: "Homework"
anchor: homework
date: 2021-03-02T14:37:41+02:00
draft: false
weight: 20
---

Homework should be submitted in pairs via Moodle. You may submit either a [Jupyter](https://jupyter.org/) (`.ipynb`) or [Pluto](https://juliapackages.com/p/pluto) (`.jl`) notebook. If your solution requires external files (data or images), put the files online and load via their URLs.

1. [Julia and Turing](/homework/01juliaturing.html), [notebook template](/homework/01juliaturing.ipynb), submission deadline **Apr 6th, 2021, 23:59.**
2. [Hierarchical models](/homework/02hier.html), [notebook template](/homework/02hier.ipynb), [City of Norfolk salaries dataset](/homework/02norfolk_employee_data.csv), submission deadline **May 18th, 2021, 23:59.** Partial solution —  City of Norfolk salaries analysis: [expanded](/homework/02norfolk-expanded.jl.html) or [folded](/homework/02norfolk-folded.jl.html).
3. [Checking comparing and evaluating models](/homework/03checkeval.html), [notebook template](/homework/03checkeval.ipynb), submission deadline **June 15th, 2021, 23:59.**
4. [Roundup](/homework/04roundup.html), [notebook template](/homework/04roundup.ipynb), [Iris data](/homework/04iris.csv), [reedfrogs data](/homework/04reedfrogs.csv), submission deadline **August 1st, 2021, 23:59.**
