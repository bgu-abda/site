--- 
title: "Links"
anchor: links
date: 2021-03-02T14:37:41+02:00
draft: false
weight: 40
---

## Communication


* [Zoom](https://us02web.zoom.us/j/82455749432?pwd=aWZVVHZ1azBzZ0FmWHVYbngreGxqUT09), Wednesdays 16:00-19:00.
* [Slack](https://bgu-abda.slack.com/) — announcements and discussion.

## Repositories

* [Slides](https://bitbucket.org/bgu-abda/slides) — slides, for offline viewing.
* [Code](https://bitbucket.org/bgu-abda/code) — code used in lectures.

## Bibliography

* [Richard McElreath. Statistical Rethinking](https://xcelab.net/rm/statistical-rethinking/)
* [Gelman et al. Bayesian Data Analysis](http://www.stat.columbia.edu/~gelman/book/)
* [Goodman and Tenenbaum. Probabilistic Models of Cognition](http://probmods.org/)
* [Bishop. Pattern Recognition and Machine Learning](https://www.microsoft.com/en-us/research/publication/pattern-recognition-machine-learning/)
* [Turing](https://turing.ml/) and [Julia](https://julialang.org)
* [Stan](https://mc-stan.org/)
* [Infergo](https://infergo.org/)
* As well as anything you find (useful) about probabilistic programming.

## Previous years

* [2020](https://www.cs.bgu.ac.il/~abda202/)
