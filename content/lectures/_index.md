---
title: "Lectures"
anchor: lectures
date: 2021-03-02T14:37:41+02:00
draft: false
weight: 10
---

1. [Introduction](/slides/00intro.html), [recording](https://youtu.be/zmVzOcgiXjA)
2. [Fundamentals](/slides/01funda.html), [recording](https://youtu.be/BGOahpopg4s)
3. [Bulding blocks](/slides/02blocks.html), [recording](https://youtu.be/fZxStFgQa_Y)
4. [Priors](/slides/03priors.html), [Approximate computation](slides/03appcmp.html), [recording](https://youtu.be/bUU5Q31jwDE)
5. [Hierarchical models](/slides/04hier.html), [recording](https://youtu.be/tko4xBJEsqI)
6. [Hierarchical models, cont.](/slides/05hierin.html), [recording](https://youtu.be/nHumCRo-6iY)
7. [Model checking](/slides/06checking.html), [recording](https://youtu.be/mq3FKALhyrc)
8. [Model evaluation](/slides/07eval.html), [recording](https://youtu.be/fxTqzf9xbPY)
9. [Regression models](/slides/08linreg.html), [recording](https://youtu.be/wfh9Vjq2p7c)
10. [Mixture models](/slides/09mixture.html), [recording](https://youtu.be/Jj0V0r2vOuA)
11. [Differential equations](/slides/10diffeq.html), [recording](https://youtu.be/owH_uvmIb_g)
12. [Roundup](/slides/11roundup.html), [recording](https://youtu.be/DAE_Xc6eTpU)
